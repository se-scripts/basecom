﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class Message
        {
            public enum ActionType
            {
                RequestPad,
                ReqPath,
                RegisterPad,
                RequestJob
            }


            public ActionType Action;
            public string action;
            public string message;
            public long sender;

            public Message(string action, string message, long sender)
            {
                this.action = action;
                this.message = message;
                this.sender = sender;
            }

            public Message(string msg, long sender)
            {
                var msgSplit = msg.Split(new char[] { ':' });

                switch (msgSplit[0]) {
                    case "RequestPad": {
                            Action = ActionType.RequestPad;
                            break;
                        }
                    case "ReqPath": {
                            Action = ActionType.ReqPath;
                            break;
                        }
                    case "RegisterPad":
                        Action = ActionType.RegisterPad;
                        break;
                    case "RequestJob":
                        Action = ActionType.RequestJob;
                        break;
                }


                this.action = msgSplit[0];
                this.message = msgSplit[1];
                this.sender = sender;

            }

            override public string ToString()
            {
                return String.Format("Action: {0};\nMessage: {1};\nSender: {2};\n", action, message, sender);
            }
        }
    }
}
