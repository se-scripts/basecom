﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRageMath;

namespace IngameScript {
    partial class Program {
        public class BasePortManager {
            public List<IMyShipConnector> Ports;

            private List<IMyShipConnector> PortsReady;

            private IMyGridTerminalSystem GridTerminalSystem;
            private IMyIntergridCommunicationSystem IGC;
            private Program Program;

            private bool ApproachBusy = false;

            public BasePortManager(List<IMyShipConnector> ports, Program program, MyIni config) {
                Ports = ports;
                PortsReady = ports.FindAll(port => port.Status == MyShipConnectorStatus.Unconnected);
                GridTerminalSystem = program.GridTerminalSystem;
                IGC = program.IGC;
                Program = program;
            }

            public IMyShipConnector RequestPort() {
                var port = PortsReady.Pop();
                return port;
            }

            public void HandleMessage(Message msg) {
                switch (msg.Action) {
                    case Message.ActionType.RequestPad: {
                            ActionRequestPad(msg);
                            break;
                        }
                    case Message.ActionType.ReqPath: {



                            var components = msg.message.Split(new char[] { ',' });
                            Vector3D vector = new Vector3D();
                            vector.X = Double.Parse(components[0]);
                            vector.Y = Double.Parse(components[1]);
                            vector.Z = Double.Parse(components[2]);

                            string routeWay = components[3];
                            FlightJob.PathType pathType;
                            if (routeWay == "H") {
                                pathType = FlightJob.PathType.Home;
                            }
                            else {
                                pathType = FlightJob.PathType.Away;
                            }



                            FlightWaypoint wp = new FlightWaypoint();
                            wp.Point = vector;
                            wp.Action = FlightWaypoint.ActionType.None;

                            Program.AddWaypoint(pathType, wp);


                            Program.DebugWrite(GenGPS("MyTest", vector));
                            break;
                        }
                    case Message.ActionType.RegisterPad: {
                            Vector3D vector = MakeVectorFromString(msg.message);
                            Program.currentFlightJob.Pad = vector;
                            break;
                        }
                    case Message.ActionType.RequestJob: {
                            var job = Program.flightJobs.Find(j => j.Name == msg.message);
                            if (job == null) return;

                            MyIni jobConfig = new MyIni();
                            jobConfig.Set("job", "name", job.Name);
                            job.Save(jobConfig, "job");

                            string jobBase64 = Convert.ToBase64String(Encoding.UTF8.GetBytes(jobConfig.ToString()));
                            Program.IGC.SendUnicastMessage(msg.sender, "NewJob", jobBase64);
                            break;
                        }
                }


            }

            private string GenGPS(string name, Vector3D pos) {
                return String.Format("GPS:{0}:{1}:{2}:{3}:\n", name, pos.X, pos.Y, pos.Z);
            }


            private void ActionRequestPad(Message msg) {
                try {
                    var port = PortsReady.First();
                    var pos = port.GetPosition();


                    // var upVector = port.WorldMatrix.Forward;
                    // pos = (upVector * 5) + pos;

                    // GPS:Askilada[DK] #1:53589.27:-26540.97:12129.47:

                    string gps = String.Format("GPS:GPS #3:{0}:{1}:{2}:\n", pos.X, pos.Y, pos.Z);


                    var upVector = port.WorldMatrix.Forward;
                    var newVector = (upVector * 10) + pos;

                    IGC.SendUnicastMessage(msg.sender, "PortUpperPoint", newVector);

                }
                catch (Exception ex) {
                    Program.DebugWrite(String.Format("An error happend:\n{0}\n", ex.ToString()));
                }
            }

            private struct RouteCord {
                Vector3D cord;
                int seq;

                public RouteCord(Vector3D vector3D, int seq) {
                    this.cord = vector3D;
                    this.seq = seq;
                }
            }

            private Vector3D MakeVectorFromString(string str) {
                var components = str.Split(new char[] { ',' });
                Vector3D vector = new Vector3D();
                vector.X = Double.Parse(components[0]);
                vector.Y = Double.Parse(components[1]);
                vector.Z = Double.Parse(components[2]);

                return vector;
            }


        }



    }
}
