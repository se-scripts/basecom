﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRageMath;

namespace IngameScript {
    partial class Program {
        public class FlightJob {
            public enum PathType {
                Home,
                Away
            }

            public string Name;

            private List<FlightWaypoint> awayPath = new List<FlightWaypoint>();
            private List<FlightWaypoint> homePath = new List<FlightWaypoint>();
            public Vector3D Pad = new Vector3D();

            public FlightJob() {

            }

            public void AddWaypoint(PathType pathType, FlightWaypoint waypoint) {
                switch(pathType) {
                    case PathType.Home:
                        homePath.Add(waypoint);
                        break;
                    case PathType.Away:
                        awayPath.Add(waypoint);
                        break;
                }
            }
            public void Save(MyIni config, string name = null) {

                string groupName;
                if (name == null)
                    groupName = Name;
                else
                    groupName = name;

                foreach (var path in homePath.Select((hp, index) => new { wp = hp, index })) {
                    config.Set(groupName, String.Format("hp{0}", path.index), path.wp.ToString());
                }

                foreach (var path in awayPath.Select((ap, index) => new { wp = ap, index})) {
                    config.Set(groupName, String.Format("ap{0}", path.index), path.wp.ToString());
                }

                config.Set(groupName, "pad", String.Format("{0},{1},{2}", Pad.X, Pad.Y, Pad.Z));

                


            }


        }


  

        public struct FlightWaypoint {
            public Vector3D Point;
            public ActionType Action;
            
            public enum ActionType {
                None,
                Dock,
                RequestPort
            }

            private string ActionToString(ActionType type) {
                switch(type) {
                    case ActionType.None:
                        return "N";
                    case ActionType.Dock:
                        return "D";
                    case ActionType.RequestPort:
                        return "RP";
                }
                return "#";
            }

            public override string ToString() {

                return String.Format("A:{0};P:{1},{2},{3}", ActionToString(Action), Point.X, Point.Y, Point.Z);

            }
        }


    }
}
