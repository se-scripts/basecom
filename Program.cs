﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRageMath;

namespace IngameScript {
    partial class Program : MyGridProgram {
        MyIni myIni = new MyIni();

        const string LANDINGPAD_GROUP_NAME = "LandingPorts";
        const string BASE_CALLBACK = "BASE_CALLBACK";

        public List<FlightJob> flightJobs = new List<FlightJob>();


        int counter = 0;

        IMyTextPanel debugPanel;
        IMyBroadcastListener listener;
        BasePortManager portManager;

        FlightJob currentFlightJob;


        public Program() {
            MyIniParseResult parseResult;
            if (!myIni.TryParse(Me.CustomData, out parseResult)) {
                Echo("Configuration parse error: " + parseResult.ToString());
                throw new Exception();
            }

            debugPanel = GridTerminalSystem.GetBlockWithName("Debug Panel") as IMyTextPanel;
            var listener = IGC.RegisterBroadcastListener("BASE");
            listener.SetMessageCallback(BASE_CALLBACK);


           



            this.listener = listener;
            Runtime.UpdateFrequency = UpdateFrequency.Update10;
            DebugWrite("Debug started\n", false);
            SetupFlightJobs();


            // Register ports
            // #LandingPorts

            var landingpadGroup = GridTerminalSystem.GetBlockGroupWithName(LANDINGPAD_GROUP_NAME);
            List<IMyShipConnector> connectors = new List<IMyShipConnector>();
            landingpadGroup.GetBlocksOfType(connectors);

            portManager = new BasePortManager(connectors, this, myIni);




        }

        public void Save() {

        }

        public void Main(string argument, UpdateType updateSource) {
            if (updateSource == UpdateType.Terminal) {

                switch (argument) {
                    case "NewPath":
                        currentFlightJob = new FlightJob();
                        DebugWrite("Creating new FlightJob\n");
                        return;

                    case "Save":
                        currentFlightJob.Save(myIni);

                        flightJobs.Add(currentFlightJob);

                        myIni.Set("main", "flightJobs", string.Join(",", flightJobs.Select(j => j.Name)));




                        Me.CustomData = myIni.ToString();
                        return;
                }

                var spl = argument.Split(new char[] { ';' }, 2);
                string action = spl[0];
                string arguments = spl[1];

                switch(action) {
                    case "SetName":
                        currentFlightJob.Name = arguments;
                        break;

                    case "AddWP":
                        break;
                }

                return;
            }


            if (argument == BASE_CALLBACK) {
                counter++;
                DebugWrite(String.Format("Got Message: {0}\nUpdate Source: {1}\n", counter, updateSource), false);

                if (listener.HasPendingMessage) {
                    var incomingMsg = listener.AcceptMessage();
                    var msg = new Message(incomingMsg.As<string>(), incomingMsg.Source);




                    DebugWrite(String.Format("Message2: {0}\n", incomingMsg.As<string>()));
                    DebugWrite("Has message? " + (msg != null).ToString(), true);
                    DebugWrite(msg.ToString());

                    portManager.HandleMessage(msg);


                }





            }
            else {
                Echo("Debug: " + counter.ToString());
            }
        }

        public void SendMessage(string msg, Nullable<long> id = null) {
            if (id != null) {
                IGC.SendUnicastMessage(id.Value, "BASE", msg);
            }
            else {
                IGC.SendBroadcastMessage("BASE", msg, TransmissionDistance.AntennaRelay);
            }

        }


        public void AddWaypoint(FlightJob.PathType pathType, FlightWaypoint waypoint) {
            currentFlightJob.AddWaypoint(pathType, waypoint);
        }

        private void DebugWrite(string msg, bool append = true) {
            debugPanel.WriteText(msg, append);
        }

        private void SetupFlightJobs() {

            List<MyIniKey> keys = new List<MyIniKey>();
            myIni.GetKeys("main", keys);
            bool hasFlightJobs = keys.Find(k => k.Name == "flightJobs") != null;
            if (!hasFlightJobs) return;

            // Getting flight job names later to be used to load the paths
            string flightJobsOpt = myIni.Get("main", "flightJobs").ToString();
            string[] flightJobNames = flightJobsOpt.Split(new char[] { ',' });

            foreach(string jobName in flightJobNames) {
                var job = new FlightJob();
                job.Name = jobName;

                List<MyIniKey> jobKeys = new List<MyIniKey>();
                myIni.GetKeys(jobName, jobKeys);

                List<FlightWaypoint> homeWaypoints = CreateWaypoints(FlightJob.PathType.Home, jobName);
                homeWaypoints.ForEach(wp => job.AddWaypoint(FlightJob.PathType.Home, wp));

                List<FlightWaypoint> awayWaypoints = CreateWaypoints(FlightJob.PathType.Away, jobName);
                awayWaypoints.ForEach(wp => job.AddWaypoint(FlightJob.PathType.Away, wp));

                MyIniKey searchKey = new MyIniKey(jobName, "pad");
                if (myIni.ContainsKey(searchKey) ) {
                    var value = myIni.Get(searchKey).ToString();
                    var components = value.Split(new char[] { ',' });

                    var vector = new Vector3D() {
                        X = double.Parse(components[0]),
                        Y = double.Parse(components[1]),
                        Z = double.Parse(components[2]),
                    };
                    job.Pad = vector;
                }

                flightJobs.Add(job);
            }

            

            string debugText = String.Format("FlightJobs loaded: [{0}]", string.Join(", ", flightJobs.Select(j => j.Name)));
            DebugWrite(debugText);
        }

        private List<FlightWaypoint> CreateWaypoints(FlightJob.PathType pathType, string jobName) {
            string searchKey;
            switch(pathType) {
                case FlightJob.PathType.Home:
                    searchKey = "hp";
                    break;

                case FlightJob.PathType.Away:
                    searchKey = "ap";
                    break;
                default:
                    throw new Exception();
            }

            List<MyIniKey> tmpKeys = new List<MyIniKey>();
            myIni.GetKeys(jobName, tmpKeys);
            List<string> pointKeys = tmpKeys.FindAll(key => key.Name.StartsWith(searchKey)).Select(key => key.Name).ToList();

            List<FlightWaypoint> flightWaypoints = new List<FlightWaypoint>();

            foreach (string key in pointKeys) {
                var iniValue = myIni.Get(jobName, key);
                if (iniValue.IsEmpty) continue;

                var wp = new FlightWaypoint();

                string[] blobs = iniValue.ToString().Split(new char[] { ';' });
                foreach (string blob in blobs) {
                    var d = blob.Split(new char[] { ':' });
                    
                    switch (d[0]) {
                        case "A":
                            if (d[1] == "D") {
                                wp.Action = FlightWaypoint.ActionType.Dock;
                            }
                            else {
                                wp.Action = FlightWaypoint.ActionType.None;
                            }

                            break;

                        case "P":
                            var cords = d[1].Split(new char[] { ',' });
                            Vector3 vector = new Vector3();
                            vector.X = float.Parse(cords[0]);
                            vector.Y = float.Parse(cords[1]);
                            vector.Z = float.Parse(cords[2]);
                            wp.Point = vector;

                            break;
                    }
                }

                flightWaypoints.Add(wp);
            }

            return flightWaypoints;
        }
    }
}